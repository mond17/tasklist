import React from 'react';
import {View, StatusBar} from 'react-native';

//@react-navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//screens
import AddTaskScreen from './screens/AddTaskScreen';
import MainScreen from './screens/MainScreen';
const Stack = createStackNavigator();

function App() {
  return (
    <View style={{flex: 1}}>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            animationEnabled: false,
          }}>
          <Stack.Screen name="Main" component={MainScreen} />
          <Stack.Screen name="AddTaskScreen" component={AddTaskScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}

export default App;
