import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Icon, Textarea} from 'native-base';
import * as Services from '../services/Services';

const {width} = Dimensions.get('window');

function AddTaskScreen({route, navigation}) {
  useEffect(() => {
    //data from mainScreen
    var {taskData} = route.params;
    var {taskText} = route.params;
    var {selectedTask} = route.params;
    setSelectedTask(selectedTask);
    setOldTask(taskData);
    if (taskText) {
      setTask(taskText);
    }
  }, [route.params]);

  //State holders
  const [task, setTask] = useState('');
  const [oldTask, setOldTask] = useState([]);
  const [SelectedTask, setSelectedTask] = useState(0);
  const {toView} = route.params;
  const [isSaveVisible, setSaveVisible] = useState(false);

  //Go back to main screen
  const goBack = () => {
    navigation.goBack();
  };

  //setState onchangetext
  const onChangeText = (value) => {
    setTask(value);
    setSaveVisible(true);
  };

  const onSave = () => {
    //for edit tasks
    if (toView) {
      //check if selected task is equal from local data
      oldTask.map((l, i) => {
        if (l.key == SelectedTask) l.text = task;
      });
      const updatedData = oldTask;
      //updating data local via AsyncStorage
      Services.storeData('tasks', updatedData).then((res) => {
        alert('Task Updated! ');
        navigation.goBack();
      });
    } else {
      //for add tasks
      var old = oldTask ? oldTask : [];
      var taskID = 0;
      if (old.length) {
        //check for the highest number *Keys from oldTask
        taskID = Math.max.apply(
          Math,
          old.map(function (item) {
            return item.key;
          }),
        );
      }
      var temp = {key: taskID + 1, text: task};
      var newTask = temp;
      old.unshift(newTask);
      //saving data local via AsyncStorage
      Services.storeData('tasks', old).then((res) => {
        alert('Task Saved! ');
        navigation.goBack();
      });
    }
  };
  function DeleteTask() {
    const filteredData = oldTask.filter((item) => item.key !== SelectedTask);
    Services.storeData('tasks', filteredData).then((res) => {
      alert('Task Deleted! ');
      navigation.goBack();
    });
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />

      <View style={{flex: 1}}>
        <View style={styles.headerContainer}>
          <BackButton toBack={goBack}>
            {isSaveVisible ? (
              toView ? (
                <Text style={styles.headerTextStyles}>Cancel</Text>
              ) : (
                <Icon name="arrow-back" style={styles.headerIconStyles} />
              )
            ) : (
              <Icon name="arrow-back" style={styles.headerIconStyles} />
            )}
          </BackButton>

          <View style={styles.headerTitleText}>
            {isSaveVisible ? (
              <Text style={styles.headerTextStyles}>Edit Task</Text>
            ) : toView ? (
              <Text style={styles.headerTextStyles}>View Task</Text>
            ) : (
              <Text style={styles.headerTextStyles}>Add new Task</Text>
            )}
          </View>
          {isSaveVisible ? (
            <SaveButton onSave={onSave}>
              <Text style={styles.headerTextStyles}>Save</Text>
            </SaveButton>
          ) : (
            <View style={{margin: 35}} />
          )}
        </View>

        <View>
          <Textarea
            bordered
            style={styles.textAreaStyles}
            rowSpan={20}
            placeholder="Please input your task here"
            value={task}
            onChangeText={onChangeText}
          />
          {toView ? (
            <FloatingButton toDelete={() => DeleteTask()}>
              <Icon
                type="MaterialIcons"
                name="delete"
                style={styles.floatButtonStyles}
              />
            </FloatingButton>
          ) : null}
        </View>
        <View style={styles.footer}></View>
      </View>
    </>
  );
}
const FloatingButton = (props) => {
  return (
    <TouchableOpacity
      onPress={props.toDelete}
      style={styles.floatButtonContainer}>
      {props.children}
    </TouchableOpacity>
  );
};
const BackButton = (props) => {
  return (
    <TouchableOpacity onPress={props.toBack} style={styles.backButtonContainer}>
      {props.children}
    </TouchableOpacity>
  );
};
const SaveButton = (props) => {
  return (
    <TouchableOpacity onPress={props.onSave} style={styles.saveButtonContainer}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  floatButtonContainer: {
    position: 'absolute',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'red',
    bottom: 0,
    right: 0,
    marginEnd: 50,
  },
  backButtonContainer: {
    paddingStart: 10,
  },
  saveButtonContainer: {
    paddingEnd: 10,
  },
  headerContainer: {
    flex: 0,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'lightseagreen',
    alignItems: 'center',
    width: width,
    height: 60,
  },
  headerTextStyles: {
    color: 'white',
    padding: 10,
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerTitleText: {
    alignSelf: 'center',
  },
  headerIconStyles: {fontSize: 30, color: 'white'},
  floatButtonStyles: {fontSize: 30, color: 'white'},
  textAreaStyles: {
    margin: 20,
    borderRadius: 10,
  },
  footer: {
    padding: 5,
  },
});

export default AddTaskScreen;
