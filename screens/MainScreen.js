import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {Icon} from 'native-base';
const {width} = Dimensions.get('window');
import * as Services from '../services/Services';

function MainScreen({navigation}) {
  useFocusEffect(
    React.useCallback(() => {
      Services.retrieveData('tasks').then((res) => {
        if (!res.status) {
          console.log('TASK LOCALLY :', res);
          setData(res);
        }
      });
    }, []),
  );
  const [data, setData] = useState([]);

  const AddTask = () => {
    navigation.navigate('AddTaskScreen', {taskData: data});
  };
  function ViewTask(param) {
    console.log('PARAMS : ', param);
    navigation.navigate('AddTaskScreen', {
      toView: true,
      selectedTask: param.key,
      taskText: param.text,
      taskData: data,
    });
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />

      <View style={{flex: 1}}>
        <View style={styles.headerContainer}>
          <Text style={styles.textHeaderStyle}>Task list</Text>
        </View>
        {data.length ? (
          <FlatList
            style={styles.taskListContainer}
            data={data}
            renderItem={({item}) => (
              <View key={item.key}>
                <TaskCard key={item.key} ViewTask={() => ViewTask(item)}>
                  <Text
                    style={{color: 'black'}}
                    numberOfLines={4}
                    ellipsizeMode="tail">
                    {item.text}
                  </Text>
                </TaskCard>
              </View>
            )}
            keyExtractor={({item}) => item}
          />
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: 30,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'gray',
              }}>
              No Task
            </Text>
          </View>
        )}
        <View style={styles.footer}></View>
        <FloatingButton toAdd={AddTask}>
          <Icon
            type="MaterialIcons"
            name="edit"
            style={styles.floatButtonStyles}
          />
        </FloatingButton>
      </View>
    </>
  );
}

const FloatingButton = (props) => {
  return (
    <TouchableOpacity
      key={props.key}
      onPress={props.toAdd}
      style={styles.floatButtonContainer}>
      {props.children}
    </TouchableOpacity>
  );
};
const TaskCard = (props) => {
  return (
    <TouchableOpacity onPress={props.ViewTask} style={styles.taskCardContainer}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  floatButtonContainer: {
    position: 'absolute',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'red',
    bottom: 0,
    right: 0,
    margin: 50,
  },
  taskListContainer: {
    padding: 10,
  },
  taskCardContainer: {
    alignSelf: 'center',
    width: '98%',
    backgroundColor: 'white',
    borderRadius: 10,
    marginBottom: 10,
    padding: 20,
    elevation: 5,
  },
  textHeaderStyle: {
    textAlign: 'left',
    padding: 20,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    backgroundColor: 'lightseagreen',
    flex: 0,
    width: width,
    height: 60,
  },
  floatButtonStyles: {fontSize: 30, color: 'white'},
  footer: {
    padding: 5,
  },
});

export default MainScreen;
