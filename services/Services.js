import AsyncStorage from '@react-native-community/async-storage';

//new saving tasks
export const storeData = async (key, data) => {
  try {
    await AsyncStorage.setItem('@' + key + ':key', JSON.stringify(data));
    return {status: 'success', reason: 'data saved', result: data};
  } catch (error) {
    return error;
    // Error saving data
  }
};

//retrieve saved tasks

export const retrieveData = async (key) => {
  // AsyncStorage.clear();
  try {
    const value = await AsyncStorage.getItem('@' + key + ':key');
    console.log('Async, Retrieved value:', value);
    if (value !== null) {
      return JSON.parse(value);
      // return value;
    } else {
      return {status: 'failed', reason: 'data does not exist'};
    }
  } catch (error) {
    return error;
    // Error retrieving data
  }
};
